data "docker_registry_image" "Lidarr" {
	name = "linuxserver/lidarr:latest"
}

resource "docker_image" "Lidarr" {
	name = data.docker_registry_image.Lidarr.name
	pull_triggers = [data.docker_registry_image.Lidarr.sha256_digest]
}

module "Lidarr" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Lidarr.latest

	networks = [{ name: var.docker_network, aliases: ["lidarr.${var.internal_domain_base}"] }]
  ports = [{ internal: 8686, external: 9606, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Lidarr"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.music}"
			container_path = "/music"
			read_only = false
		},
		{
			host_path = "${var.downloads}"
			container_path = "/downloads"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}